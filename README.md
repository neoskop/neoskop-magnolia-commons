# README

Allgemeine Beschreibung vom Modul **Neoskop Magnolia Commons Module**.

# Abhängigkeiten

Bis Version 1.3.2:

- [Magnolia CMS][1] >= 5.3.9

Ab Version 1.4.0:

- [Magnolia CMS][1] >= 5.6.7

# Installation

Das Modul muss in der `pom.xml` des Magnolia-Projektes als Abhängigkeit hinzugefügt werden:

```xml
<dependency>
	<groupId>org.bitbucket.neoskop</groupId>
	<artifactId>neoskop-magnolia-commons</artifactId>
	<version>1.3.2</version>
</dependency>
```

# Verwendung des Moduls

Das Modul stellt eine Utility-Sammlung für das Magnolia-CMS bereit. Zur Zeit sind folgende Funktionen implementiert:

- Setzen des Superuser-Passworts beim Starten des Magnolia-Systems: Das Magnolia mit dem System-Parameter **neoskop.magnolia.superuser.password=meinPasswort** starten
- Entsperren des Superuser-Accounts beim Starten des Magnolia-Systems: Das Magnolia mit dem System-Parameter **neoskop.magnolia.superuser.unlock=true** starten
- Live-Vorschau-Action für die Pages-App

Mehr Informationen zur Benutzung

[1]: https://www.magnolia-cms.com
[2]: http://maven.neoskop.io
[3]: http://maven.apache.org/maven-release/maven-release-plugin/
