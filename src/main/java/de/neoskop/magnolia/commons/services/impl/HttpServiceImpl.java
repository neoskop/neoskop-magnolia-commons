package de.neoskop.magnolia.commons.services.impl;

import de.neoskop.magnolia.commons.services.HttpService;
import info.magnolia.context.MgnlContext;
import javax.servlet.http.HttpServletRequest;

public class HttpServiceImpl implements HttpService {

  private static final String HTTP = "http://";
  private static final String HTTPS = "https://";
  private static final String X_FORWARDED_HOST = "X-Forwarded-Host";

  /**
   * Returns the name of the host serving the given request. If a X-Forwarded-Host-Header is present
   * (e.g. running behind proxy server), it will be returned, otherwise request.getServerName()
   * (with optional port) is returned.
   */
  @Override
  public String getHost() {
    HttpServletRequest request = MgnlContext.getWebContext().getRequest();

    if (request.getHeader(X_FORWARDED_HOST) != null) {
      return request.getHeader(X_FORWARDED_HOST);
    } else {
      return request.getServerName()
          + (request.getServerPort() != 80 && request.getServerPort() != 443
              ? ":" + request.getServerPort()
              : "");
    }
  }

  /**
   * Returns the plain name (without port) of the host serving the given request. If a
   * X-Forwarded-Host-Header is present (e.g. running behind proxy server), it will be returned,
   * otherwise request.getServerName() is returned.
   */
  @Override
  public String getServerName() {
    HttpServletRequest request = MgnlContext.getWebContext().getRequest();

    if (request.getHeader(X_FORWARDED_HOST) != null) {
      return request.getHeader(X_FORWARDED_HOST);
    } else {
      return request.getServerName();
    }
  }

  /** Returns the url of the host serving the given request. schema://authority[:port] */
  @Override
  public String getServerURL() {
    HttpServletRequest request = MgnlContext.getWebContext().getRequest();
    return (request.isSecure() ? HTTPS : HTTP) + getHost();
  }

  /** Returns the server url with context path for the given request. */
  @Override
  public String getContextURL() {
    HttpServletRequest request = MgnlContext.getWebContext().getRequest();
    return (request.isSecure() ? HTTPS : HTTP) + getHost() + request.getContextPath();
  }
}
