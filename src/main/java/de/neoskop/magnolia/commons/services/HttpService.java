package de.neoskop.magnolia.commons.services;

public interface HttpService {

  String getHost();

  String getServerName();

  String getServerURL();

  String getContextURL();
}
