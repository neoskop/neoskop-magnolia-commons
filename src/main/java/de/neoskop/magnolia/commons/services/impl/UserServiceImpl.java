package de.neoskop.magnolia.commons.services.impl;

import de.neoskop.magnolia.commons.services.UserService;
import info.magnolia.cms.security.SecuritySupport;
import info.magnolia.cms.security.User;
import info.magnolia.cms.security.UserManager;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserServiceImpl implements UserService {

  private static final String SUPERUSER = "superuser";
  private static final String SYSTEM_REALM = "system";

  private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

  @Inject private SecuritySupport securitySupport;

  @Override
  public void changeSuperuserPassword(String newPassword) {
    if (newPassword != null) {
      UserManager userManager = securitySupport.getUserManager(SYSTEM_REALM);
      User superUser = userManager.getUser(SUPERUSER);
      userManager.changePassword(superUser, newPassword);

      logger.info("superuser password changed");
    }
  }

  @Override
  public void unlockSuperuserAccount() {
    UserManager userManager = securitySupport.getUserManager(SYSTEM_REALM);
    User superUser = userManager.getUser(SUPERUSER);
    userManager.setProperty(superUser, "enabled", "true");

    logger.info("superuser account unlocked");
  }
}
