package de.neoskop.magnolia.commons.services;

public interface UserService {

  void changeSuperuserPassword(String newPassword);

  void unlockSuperuserAccount();
}
