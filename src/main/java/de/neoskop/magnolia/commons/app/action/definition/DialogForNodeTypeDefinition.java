package de.neoskop.magnolia.commons.app.action.definition;

/** Created by cbinzer on 09.03.17. */
public class DialogForNodeTypeDefinition {

  private String nodeType;
  private String dialogName;

  public String getNodeType() {
    return nodeType;
  }

  public void setNodeType(String nodeType) {
    this.nodeType = nodeType;
  }

  public String getDialogName() {
    return dialogName;
  }

  public void setDialogName(String dialogName) {
    this.dialogName = dialogName;
  }
}
