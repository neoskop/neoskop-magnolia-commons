package de.neoskop.magnolia.commons.app.action.definition;

import de.neoskop.magnolia.commons.app.action.OpenEditDialogForNodeTypeAction;
import info.magnolia.ui.api.action.ConfiguredActionDefinition;
import java.util.List;

/** Created by cbinzer on 09.03.17. */
public class OpenEditDialogForNodeTypesActionDefinition extends ConfiguredActionDefinition {

  private List<DialogForNodeTypeDefinition> dialogsForNodeTypes;

  public OpenEditDialogForNodeTypesActionDefinition() {
    setImplementationClass(OpenEditDialogForNodeTypeAction.class);
  }

  public List<DialogForNodeTypeDefinition> getDialogsForNodeTypes() {
    return dialogsForNodeTypes;
  }

  public void setDialogsForNodeTypes(List<DialogForNodeTypeDefinition> dialogsForNodeTypes) {
    this.dialogsForNodeTypes = dialogsForNodeTypes;
  }
}
