package de.neoskop.magnolia.commons.app.action.definition;

import de.neoskop.magnolia.commons.app.action.PreviewPageAction;
import info.magnolia.ui.api.action.ConfiguredActionDefinition;

/** Created by cbinzer on 02.05.16. */
public class PreviewPageActionDefinition extends ConfiguredActionDefinition {

  public PreviewPageActionDefinition() {
    setImplementationClass(PreviewPageAction.class);
  }
}
