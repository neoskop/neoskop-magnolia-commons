package de.neoskop.magnolia.commons.app.action;

import com.vaadin.v7.data.Item;
import de.neoskop.magnolia.commons.app.action.definition.DialogForNodeTypeDefinition;
import de.neoskop.magnolia.commons.app.action.definition.OpenEditDialogForNodeTypesActionDefinition;
import info.magnolia.event.EventBus;
import info.magnolia.ui.api.action.AbstractAction;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.event.ContentChangedEvent;
import info.magnolia.ui.dialog.formdialog.FormDialogPresenter;
import info.magnolia.ui.dialog.formdialog.FormDialogPresenterFactory;
import info.magnolia.ui.form.EditorCallback;
import info.magnolia.ui.vaadin.integration.contentconnector.ContentConnector;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import java.util.Optional;
import javax.inject.Named;
import org.apache.commons.lang3.StringUtils;

/** Created by cbinzer on 09.03.17. */
public class OpenEditDialogForNodeTypeAction
    extends AbstractAction<OpenEditDialogForNodeTypesActionDefinition> {

  private Item item;
  private UiContext uiContext;
  private EventBus eventBus;
  private ContentConnector contentConnector;
  private OpenEditDialogForNodeTypesActionDefinition definition;
  private FormDialogPresenterFactory formDialogPresenterFactory;

  public OpenEditDialogForNodeTypeAction(
      OpenEditDialogForNodeTypesActionDefinition definition,
      Item itemToEdit,
      FormDialogPresenterFactory formDialogPresenterFactory,
      UiContext uiContext,
      @Named("admincentral") EventBus eventBus,
      ContentConnector contentConnector) {

    super(definition);

    this.item = itemToEdit;
    this.uiContext = uiContext;
    this.definition = definition;
    this.contentConnector = contentConnector;
    this.formDialogPresenterFactory = formDialogPresenterFactory;
    this.eventBus = eventBus;
  }

  @Override
  public void execute() throws ActionExecutionException {
    StringBuilder dialogName = new StringBuilder();

    if (item instanceof JcrNodeAdapter) {
      JcrNodeAdapter nodeAdapter = (JcrNodeAdapter) item;
      String nodeType = nodeAdapter.getPrimaryNodeTypeName();
      Optional<DialogForNodeTypeDefinition> dialogNodeTypeDefinition =
          definition
              .getDialogsForNodeTypes()
              .stream()
              .filter(dialogNodeType -> dialogNodeType.getNodeType().equals(nodeType))
              .findFirst();

      dialogNodeTypeDefinition.ifPresent(d -> dialogName.append(d.getDialogName()));
    }

    if (StringUtils.isNotBlank(dialogName.toString())) {
      final Object itemId = contentConnector.getItemId(item);
      final FormDialogPresenter formDialogPresenter =
          formDialogPresenterFactory.createFormDialogPresenter(dialogName.toString());

      if (formDialogPresenter != null) {
        formDialogPresenter.start(
            item,
            dialogName.toString(),
            uiContext,
            new EditorCallback() {

              @Override
              public void onSuccess(String actionName) {
                eventBus.fireEvent(new ContentChangedEvent(itemId));
                formDialogPresenter.closeDialog();
              }

              @Override
              public void onCancel() {
                formDialogPresenter.closeDialog();
              }
            });
      }
    }
  }
}
