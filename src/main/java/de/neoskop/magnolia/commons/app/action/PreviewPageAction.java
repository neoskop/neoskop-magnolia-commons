package de.neoskop.magnolia.commons.app.action;

import com.vaadin.ui.UI;
import de.neoskop.magnolia.commons.app.action.definition.PreviewPageActionDefinition;
import info.magnolia.context.MgnlContext;
import info.magnolia.ui.api.action.AbstractAction;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemAdapter;
import javax.inject.Inject;
import javax.jcr.RepositoryException;

/** Created by cbinzer on 02.05.16. */
public class PreviewPageAction<D extends PreviewPageActionDefinition> extends AbstractAction<D> {

  private JcrItemAdapter item;

  @Inject
  protected PreviewPageAction(D definition, JcrItemAdapter item) {
    super(definition);
    this.item = item;
  }

  @Override
  public void execute() throws ActionExecutionException {
    try {
      UI.getCurrent()
          .getPage()
          .open(MgnlContext.getContextPath() + item.getJcrItem().getPath(), "_blank");
    } catch (RepositoryException e) {
      throw new ActionExecutionException(e);
    }
  }
}
