package de.neoskop.magnolia.commons;

import de.neoskop.magnolia.commons.services.UserService;
import info.magnolia.init.MagnoliaConfigurationProperties;
import info.magnolia.module.ModuleLifecycle;
import info.magnolia.module.ModuleLifecycleContext;
import info.magnolia.objectfactory.Components;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.inject.Inject;

/**
 * This class is optional and represents the configuration for the ${module-name} module. By
 * exposing simple getter/setter/adder methods, this bean can be configured via content2bean using
 * the properties and node from <tt>config:/modules/${module-name}</tt>. If you don't need this,
 * simply remove the reference to this class in the module descriptor xml.
 */
public class NeoskopCommonsModule implements ModuleLifecycle {
  private static final Logger LOG = LoggerFactory.getLogger(NeoskopCommonsModule.class);
  private static final String SUPERUSER_PASSWORD_PROP = "neoskop.magnolia.superuser.password";
  private static final String SUPERUSER_UNLOCK_PROP = "neoskop.magnolia.superuser.unlock";

  @Inject
  private UserService userService;

  @Override
  public void start(ModuleLifecycleContext moduleLifecycleContext) {
    LOG.info("Starting module neoskop-commons");
    String newSuperuserPassword = System.getProperty(SUPERUSER_PASSWORD_PROP);

    if (StringUtils.isBlank(newSuperuserPassword)) {
      newSuperuserPassword = Components.getComponent(MagnoliaConfigurationProperties.class)
          .getProperty(SUPERUSER_PASSWORD_PROP);
    }

    userService.changeSuperuserPassword(newSuperuserPassword);

    boolean unlockSuperuserAccount =
        Boolean.valueOf(System.getProperty("neoskop.magnolia.superuser.unlock"));
    if (unlockSuperuserAccount == false) {
      unlockSuperuserAccount = Boolean.valueOf(Components
          .getComponent(MagnoliaConfigurationProperties.class).getProperty(SUPERUSER_UNLOCK_PROP));
    }

    if (unlockSuperuserAccount) {
      userService.unlockSuperuserAccount();
    }
  }

  @Override
  public void stop(ModuleLifecycleContext moduleLifecycleContext) {}
}
